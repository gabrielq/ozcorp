package br.com.ozcorp;

/**
 * 
 * @author Gabriel Quintas Baeta
 *
 */

public class Departamento {

	// atributos
	private String nome;
	private String sigla;

	private Cargo cargo;

	// construtor
	public Departamento(String nome, String sigla, Cargo cargo) {
		super();
		this.nome = nome;
		this.sigla = sigla;
		this.cargo = cargo;
	}

	// getters e setters
	// NOME
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	// SIGLA
	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	// CARGO
	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
}