package br.com.ozcorp;

/**
 * 
 * @author Gabriel Quintas Baeta
 *
 */

public class Funcionario {

	// atributos
	private String nome;
	private String rg;
	private String cpf;
	private String matricula;
	private String email;
	private String senha;
	private Departamento departamento;
	private TipoSanguineo sangue;
	private Sexo sexo;

	public Funcionario(String nome, String rg, String cpf, String matricula, String email, String senha,
			Departamento departamento, TipoSanguineo sangue, Sexo sexo) {
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.email = email;
		this.senha = senha;
		this.departamento = departamento;
		this.sangue = sangue;
		this.sexo = sexo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public TipoSanguineo getSangue() {
		return sangue;
	}

	public void setSangue(TipoSanguineo sangue) {
		this.sangue = sangue;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

}