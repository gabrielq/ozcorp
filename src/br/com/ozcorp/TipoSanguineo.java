package br.com.ozcorp;

/**
 * 
 * @author Gabriel Quintas Baeta
 *
 */

public enum TipoSanguineo {
	AMAIS("A+"),
	AMENOS("A-"),
	BMAIS("B+"),
	BMENOS("B-"),
	ABMAIS("AB+"),
	ABMENOS("AB-"),
	OMAIS("O+"),
	OMENOS("O-");
	
	public String sangue;
	
	TipoSanguineo(String sangue) {
		this.sangue = sangue;
	}
}