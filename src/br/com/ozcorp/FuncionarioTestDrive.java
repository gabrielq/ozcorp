package br.com.ozcorp;

/**
 * 
 * @author Gabriel Quintas Baeta
 *
 */

public class FuncionarioTestDrive {

	public static void main(String[] args) {

		// INSTANCIANDO FUNCIONÁRIOS
		// ANALISTA
		Analista marcela = new Analista("Marcela", "10.125.632-1", "285.558.459.63", "DSN013201", "marcela@ianes.com",
				"marcela@132", new Departamento("Departamento Financeiro", "DF", new Cargo("Analista", 1500.00)),
				TipoSanguineo.ABMAIS, Sexo.FEMININO);

		// DIRETOR
		Diretor marcos = new Diretor("Marcos", "10.125.489-6", "123.456.789.62", "DSN013202", "marcos@ianes.com",
				"marcos@132", new Departamento("Departamento Administrativo", "DA", new Cargo("Diretor", 2100.00)),
				TipoSanguineo.OMAIS, Sexo.MASCULINO);

		// ENGENHEIRO
		Engenheiro manuela = new Engenheiro("Manuela", "10.256.321-5", "123.456.987.52", "DSN013203", "manuela@ianes.com",
				"manuela@132", new Departamento("Departamento de Engenharia", "DE", new Cargo("Engenheira", 1800.00)),
				TipoSanguineo.BMENOS, Sexo.FEMININO);

		// GERENTE
		Gerente gustavo = new Gerente("Gustavo", "10.256.398-7", "145.896.325.47", "DSN013204", "gustavo@ianes.com",
				"gustavo@132", new Departamento("Departamento Administrativo", "DA", new Cargo("Gerente", 1600.00)),
				TipoSanguineo.ABMENOS, Sexo.MASCULINO);

		// SECRETARIO
		Secretario gabriel = new Secretario("Gabriel", "39.562.145-8", "359.654.159.20", "DSN013205", "gabriel@ianes.com",
				"gabriel@132", new Departamento("Departamento Administrativo", "DA", new Cargo("Secretário", 1300.00)),
				TipoSanguineo.OMENOS, Sexo.MASCULINO);

		// DADOS DOS FUNCIONÁRIOS
		// ANALISTA
		System.out.println("   DADOS DO FUNCIONÁRIO    ");
		System.out.println("Nome:            " + marcela.getNome());
		System.out.println("RG:              " + marcela.getRg());
		System.out.println("CPF:             " + marcela.getCpf());
		System.out.println("Matrícula:       " + marcela.getMatricula());
		System.out.println("E-mail:          " + marcela.getEmail());
		System.out.println("Senha:           " + marcela.getSenha());
		System.out.println("Departamento:    " + marcela.getDepartamento().getNome() + " ("
				+ marcela.getDepartamento().getSigla() + ")");
		System.out.println("Cargo:           " + marcela.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario Base:    " + marcela.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguíneo:  " + marcela.getSangue().sangue);
		System.out.println("Sexo:            " + marcela.getSexo().genero);
		System.out.println();

		// DIRETOR
		System.out.println("   DADOS DO FUNCIONÁRIO    ");
		System.out.println("Nome:            " + marcos.getNome());
		System.out.println("RG:              " + marcos.getRg());
		System.out.println("CPF:             " + marcos.getCpf());
		System.out.println("Matrícula:       " + marcos.getMatricula());
		System.out.println("E-mail:          " + marcos.getEmail());
		System.out.println("Senha:           " + marcos.getSenha());
		System.out.println("Departamento:    " + marcos.getDepartamento().getNome() + " ("
				+ marcos.getDepartamento().getSigla() + ")");
		System.out.println("Cargo:           " + marcos.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario Base:    " + marcos.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguíneo:  " + marcos.getSangue().sangue);
		System.out.println("Sexo:            " + marcos.getSexo().genero);
		System.out.println();

		// ENGENHEIRO
		System.out.println("   DADOS DO FUNCIONÁRIO    ");
		System.out.println("Nome:            " + manuela.getNome());
		System.out.println("RG:              " + manuela.getRg());
		System.out.println("CPF:             " + manuela.getCpf());
		System.out.println("Matrícula:       " + manuela.getMatricula());
		System.out.println("E-mail:          " + manuela.getEmail());
		System.out.println("Senha:           " + manuela.getSenha());
		System.out.println("Departamento:    " + manuela.getDepartamento().getNome() + " ("
				+ manuela.getDepartamento().getSigla() + ")");
		System.out.println("Cargo:           " + manuela.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario Base:    " + manuela.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguíneo:  " + manuela.getSangue().sangue);
		System.out.println("Sexo:            " + manuela.getSexo().genero);
		System.out.println();

		// GERENTE
		System.out.println("   DADOS DO FUNCIONÁRIO    ");
		System.out.println("Nome:            " + gustavo.getNome());
		System.out.println("RG:              " + gustavo.getRg());
		System.out.println("CPF:             " + gustavo.getCpf());
		System.out.println("Matrícula:       " + gustavo.getMatricula());
		System.out.println("E-mail:          " + gustavo.getEmail());
		System.out.println("Senha:           " + gustavo.getSenha());
		System.out.println("Departamento:    " + gustavo.getDepartamento().getNome() + " ("
				+ gustavo.getDepartamento().getSigla() + ")");
		System.out.println("Cargo:           " + gustavo.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario Base:    " + gustavo.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguíneo:  " + gustavo.getSangue().sangue);
		System.out.println("Sexo:            " + gustavo.getSexo().genero);
		System.out.println();

		// SECRETARIO
		System.out.println("   DADOS DO FUNCIONÁRIO    ");
		System.out.println("Nome:            " + gabriel.getNome());
		System.out.println("RG:              " + gabriel.getRg());
		System.out.println("CPF:             " + gabriel.getCpf());
		System.out.println("Matrícula:       " + gabriel.getMatricula());
		System.out.println("E-mail:          " + gabriel.getEmail());
		System.out.println("Senha:           " + gabriel.getSenha());
		System.out.println("Departamento:    " + gabriel.getDepartamento().getNome() + " ("
				+ gabriel.getDepartamento().getSigla() + ")");
		System.out.println("Cargo:           " + gabriel.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario Base:    " + gabriel.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguíneo:  " + gabriel.getSangue().sangue);
		System.out.println("Sexo:            " + gabriel.getSexo().genero);
		System.out.println();
	}

}