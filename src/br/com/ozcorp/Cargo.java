package br.com.ozcorp;

/**
 * 
 * @author Gabriel Quintas Baeta
 *
 */

public class Cargo {

	// atributos
	private String titulo;
	private double salarioBase;

	// construtor
	public Cargo(String titulo, double salarioBase) {
		super();
		this.titulo = titulo;
		this.salarioBase = salarioBase;
	}

	// getters e setters
	// T�TULO
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	// SAL�RIO BASE
	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}

}