package br.com.ozcorp;

/**
 * 
 * @author Gabriel Quintas Baeta
 *
 */

public enum Sexo {
	MASCULINO("Masculino"),
	FEMININO("Feminino"),
	OUTRO("Outro");
	
	public String genero;
	
	Sexo(String genero) {
		this.genero = genero;
	}
}