package br.com.ozcorp;

/**
 * 
 * @author Gabriel Quintas Baeta
 *
 */

public class Diretor extends Funcionario {

	// Heran�a de Funcion�rio
	public static final int NIVEL_ACESSO = 0;
	
	public Diretor(String nome, String rg, String cpf, String matricula, String email, String senha,
			Departamento departamento, TipoSanguineo sangue, Sexo sexo) {
		super(nome, rg, cpf, matricula, email, senha, departamento, sangue, sexo);
	}

}